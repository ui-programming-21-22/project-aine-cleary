//for the canvas
const canvas = document.getElementById("the_canvas")
const context = canvas.getContext("2d");

//variables
let fillVal = 0;
let gameOver = false;
let speed = 2; 
let keyPressed = "default";
let keys = 0;
let x = Math.floor(Math.random()*13);
let keyFound = false;
let gameWon = false;
let currentLoopIndex = 0;
let currentLoopIndex2 = 0;
let frameCount = 0;
let frameCount2 = 0;
let playerDirection = 0;
let enemyDirection = 0;
const width = 32;
const height = 32;
const secondImages = 3;
const startImage2 = width * secondImages;
const pos1 = {xpos:100, ypos:100, display:0};
const pos2 = {xpos:380, ypos:200, display:0};
const pos3 = {xpos:690, ypos:40, display:0};
const pos4 = {xpos:840, ypos:430, display:0};
const pos5 = {xpos:720, ypos:270, display:0};
const pos6 = {xpos:120, ypos:120, display:0};
const pos7 = {xpos:100, ypos:400, display:0}; 
const pos8 = {xpos:500, ypos:100, display:0};
const pos9 = {xpos:360, ypos:230, display:0};
const pos10 = {xpos:440, ypos:300, display:0};
const pos11 = {xpos:900, ypos:380, display:0};
const pos12 = {xpos:930, ypos:160, display:0};
const pos13 = {xpos:220, ypos:295, display:0};
const walkLoop = [0, 1, 2, 0];
const frameLimit = 7;

// Default GamerInput is set to None
let gamerInput = new GamerInput("None"); //No Input

// GameObject holds positional information
// Can be used to hold other information based on requirements
function GameObject(spritesheet, x, y, width, height) {
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.mvmtDirection = "None";
}  

// The GamerInput is an Object that holds the Current
// GamerInput (Left, Right, Up, Down, MouseClicks)
function GamerInput(input) {
    this.action = input; // Hold the current input as a string
}

//images
let bg = new Image();
bg.src = "assets/img/sample-bg.jpg";
let endScreen = new Image();
endScreen.src = "assets/img/game-over.jpg";
let winScreen = new Image();
winScreen.src = "assets/img/u-won.png";
let image = new Image();
image.src = "assets/img/wizard.png";
let keyimg = new Image();
keyimg.src = "assets/img/key.png";
let gateimg = new Image();
gateimg.src = "assets/img/gate.png";

//setting game objects
let player = new GameObject(image, 545, 245, 100, 100);
let enemy = new GameObject(image, 1000, 20, 100, 100);
let key = new GameObject(keyimg, 500, 200, 100, 100);
let gate = new GameObject(gateimg, 500, 20, 100, 100);


//nipple code
//nipple active when modal appears
//make only work when no modal displayed
let dynamic = nipplejs.create({
    color: 'orange',
    zone: document.getElementById("the_canvas")
});

dynamic.on('added', function (evt, nipple){
    nipple.on('dir:up', function (evt, data){
        //console.log("direction up");
        gamerInput = new GamerInput("Up");
    });
    nipple.on('dir:down', function (evt, data){
        //console.log("direction down");
        gamerInput = new GamerInput("Down");
    });
    nipple.on('dir:left', function (evt, data){
        //console.log("direction left");
        gamerInput = new GamerInput("Left");
    });
    nipple.on('dir:right', function (evt, data){
        //console.log("direction right");
        gamerInput = new GamerInput("Right");
    });
    nipple.on('end', function (evt, data){
        //console.log("move stopped");
        gamerInput = new GamerInput("None");
    });
});


//function to animate player
function animate(){
    if (gamerInput.action != "None"){
        frameCount++;
        if(frameCount >= frameLimit){
            frameCount = 0;
            currentLoopIndex++;
            if(currentLoopIndex >= walkLoop.length){
                currentLoopIndex = 0;
            }
        }
    }
    else{
        currentLoopIndex = 0;
    } 
}


//function to animate enemy
function animateEnemy(){
    frameCount2++;
    //console.log(frameCount2);
    if(frameCount2 >= frameLimit){
        frameCount2 = 0;
        currentLoopIndex2++;
        if(currentLoopIndex2 >= walkLoop.length){
            currentLoopIndex2 = 0;
        }
    } 
}


//function to check keyboard inputs
function input(event) {
    // Take Input from the Player
    // console.log("Input");
    // console.log("Event type: " + event.type);
    // console.log("Keycode: " + event.keyCode);
    if (event.type === "keydown") {
        switch (event.keyCode) {
            case 13: //enter key
                gamerInput = new GamerInput("Enter");
                break;
            case 37: // Left Arrow
                gamerInput = new GamerInput("Left");
                break; //Left key
            case 38: // Up Arrow
                gamerInput = new GamerInput("Up");
                break; //Up key
            case 39: // Right Arrow
                gamerInput = new GamerInput("Right");
                break; //Right key
            case 40: // Down Arrow
                gamerInput = new GamerInput("Down");
                break; //Down key
            default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } else {
        gamerInput = new GamerInput("None");
    }
}

//function to move main player around canvas
//using arrow keys on keyboard
function movePlayer(){
    if(keyPressed == "up"){
        player.y -= speed; //move player up
        keyPressed = "default"
        playerDirection = 3;
    }
    else if(keyPressed == "down"){
        player.y += speed; //move player down
        keyPressed = "default";
        playerDirection = 0;
    }
    else if(keyPressed == "left"){
        player.x -= speed; //move player left
        keyPressed = "default";
        playerDirection = 1;
    }
    else if(keyPressed == "right"){
        player.x += speed; //move player right
        keyPressed = "default";
        playerDirection = 2;
    }
    //bounderies check
    if(player.y <= 20){
        player.y = 21; //top of the screen
    }
    else if(player.y + height >= 500){
        player.y = 499 - height; //bottom of screen
    }
    else if(player.x <= 0){
        player.x = 1; //left of screen
    }
    else if(player.x + width >= 1100){
        player.x = 1099 - width;
    }
}

//function to make enemy follow player
function moveEnemy(){
    if(player.x < enemy.x){
        enemy.x -= 0.8; //move enemy left (speed was 1)
        enemyDirection = 1;
    }
    
    if(player.x > enemy.x){
        enemy.x += 0.8;//move enemy right
        enemyDirection = 2;
    }

    if(player.y < enemy.y){
        enemy.y -= 0.8;//move enemy up
        enemyDirection = 3;
    }

    if(player.y > enemy.y){
        enemy.y += 0.8;//move enemy down
        enemyDirection = 0;
    }
}

//function to update game when inputs are 
function update(){
    if (gamerInput.action === "Up") {
        keyPressed = "up";
        console.log(keyPressed);
    } else if (gamerInput.action === "Down") {
        keyPressed = "down";
        console.log(keyPressed);
    } else if (gamerInput.action === "Left") {
        keyPressed = "left";
        console.log(keyPressed);
    } else if (gamerInput.action === "Right") {
        keyPressed = "right";
        console.log(keyPressed);
    }
}

//function for random key position
function randKey(){
    console.log(x);
    //set x + y pos from objects
    //figure out how to change display in object
    //check if 0 first though
    switch(x){
        case 0: //rand is 0
            if(pos1.display == 0){ //check if displayed already
                pos1.display = 1;
                key.x = pos1.xpos;
                key.y = pos1.ypos;
                x = Math.floor(Math.random()*13);
            }
            break;
        case 1: //rand is 1
            if(pos2.display == 0){ //check if displayed already
                pos2.display = 1;
                key.x = pos2.xpos;
                key.y = pos2.ypos;
                x = Math.floor(Math.random()*13);
            }
            break;
        case 2: //rand is 2
            if(pos3.display == 0){ //check if displayed already
                pos3.display = 1;
                key.x = pos3.xpos;
                key.y = pos3.ypos;
                x = Math.floor(Math.random()*13);
            }
            break;
        case 3: //rand is 3
            if(pos4.display == 0){ //check if displayed already
                pos4.display = 1;
                key.x = pos4.xpos;
                key.y = pos4.ypos;
                x = Math.floor(Math.random()*13);
            }
            break;
        case 4: //rand is 4
            if(pos5.display == 0){ //check if displayed already
                pos5.display = 1;
                key.x = pos5.xpos;
                key.y = pos5.ypos;
                x = Math.floor(Math.random()*13);
            }
            break;
        case 5: //rand is 5
            if(pos6.display == 0){ //check if displayed already
                pos6.display = 1;
                key.x = pos6.xpos;
                key.y = pos6.ypos;
                x = Math.floor(Math.random()*13);
            }
            break;
        case 6: //rand is 6
            if(pos7.display == 0){ //check if displayed already
                pos7.display = 1;
                key.x = pos7.xpos;
                key.y = pos7.ypos;
                x = Math.floor(Math.random()*13);
            }
            break;
        case 7: //rand is 7
            if(pos8.display == 0){ //check if displayed already
                pos8.display = 1;
                key.x = pos8.xpos;
                key.y = pos8.ypos;
                x = Math.floor(Math.random()*13);
            }
            break;
        case 8: //rand is 8
            if(pos9.display == 0){ //check if displayed already
                pos9.display = 1;
                key.x = pos9.xpos;
                key.y = pos9.ypos;
                x = Math.floor(Math.random()*13);
            }
            break;
        case 9: //rand is 9
            if(pos10.display == 0){ //check if displayed already
                pos10.display = 1;
                key.x = pos10.xpos;
                key.y = pos10.ypos;
                x = Math.floor(Math.random()*13);
            }
            break;
        case 10: //rand is 10
            if(pos11.display == 0){ //check if displayed already
                pos11.display = 1;
                key.x = pos11.xpos;
                key.y = pos11.ypos;
                x = Math.floor(Math.random()*13);
            }
            break;
        case 11: //rand is 11
            if(pos12.display == 0){ //check if displayed already
                pos12.display = 1;
                key.x = pos12.xpos;
                key.y = pos12.ypos;
                x = Math.floor(Math.random()*13);
            }
            break;
        case 12: //rand is 12
            if(pos13.display == 0){ //check if displayed already
                pos13.display = 1;
                key.x = pos13.xpos;
                key.y = pos13.ypos;
                x = Math.floor(Math.random()*13);
            }
            break;
        default:
            key.x = -20;
            key.y = -20;
    }
}

//drawing a timer bar
function drawTimer(){
   let width = 1100;
   let height = 20;

   //drawing background
   context.fillStyle = "#000000";
   context.clearRect(0, 0, canvas.width, canvas.height);
   context.fillRect(0, 0, width, height);

   //drawing fill
   context.fillStyle = "#00ff00";
   context.fillRect(0, 0, fillVal*width, height);
   fillVal += 0.0002;

   if(fillVal >= 1.0){ //1.0 is good time
    gameOver = true;
   }
}

//drwaing score on canvas
function writeScore(){
    let scorestring = "score: " + scoreCount;
    context.font = '22px sans-serif';
    context.fillText(scorestring, 1000, 480);
}

//checking for collision of player + enemy/keys
function collision(){
    //enemy collision
    if(enemy.x < player.x + width && enemy.x + width > player.x
        && enemy.y < player.y + height && enemy.y + height > player.y){
            //console.log("collision");
            if(scoreCount > 0){
                scoreCount--; //only looses key if over 0
            }
            localStorage.setItem("score", scoreCount);
            player.x = 545;
            player.y = 245;
            enemy.x = 1000;
            enemy.y = 20;
        }
    //collision for key
    if(key.x < player.x + width && key.x + width > player.x
        && key.y < player.y + height && key.y + height > player.y){
            console.log("collect key")
            keyFound = true;
            scoreCount++;
            localStorage.setItem("score", scoreCount);
        }
    //collision for gate
    if(gate.x < player.x + width && gate.x + width > player.x
        && gate.y < player.y + height && gate.y + height > player.y){
            //check if enough keys found
            if(scoreCount >= 5){
                gameWon = true;
            }
        }
}

function draw(){
    context.clearRect(0, 0, canvas.width, canvas.height);
    drawTimer();
    context.drawImage(bg, 0, 20, canvas.width, canvas.height);
    writeScore();
    //player
    context.drawImage(player.spritesheet, (walkLoop[currentLoopIndex] * width), 
    (playerDirection * height), width, height, player.x, player.y, width, height);

    //enemy
    context.drawImage(enemy.spritesheet, startImage2 + (width * walkLoop[currentLoopIndex2]), 
    (enemyDirection * height), width, height, enemy.x, enemy.y, width, height);

    context.drawImage(key.spritesheet, key.x, key.y, width, height);
    context.drawImage(gate.spritesheet, gate.x, gate.y, (48 * 1.5), (height * 1.5));
    //check for game over to draw new screen
    if(gameOver){
        context.drawImage(endScreen, 0, 0, canvas.width, canvas.height);
    }
    //check for game won to draw new screen
    if(gameWon){
        context.fillStyle = "black";
        context.fillRect(0, 0, canvas.width, canvas.height);
        context.drawImage(winScreen, 170, 0, canvas.height + 260, canvas.height);
    }
}

//the game loop - 
// calling the request animation frame again to make the loop
function gameloop() {
    draw();
    update();
    
    if(!gameOver){
        if(!gameWon){
            movePlayer();
            moveEnemy();
            animate();
            animateEnemy();
            collision();
            if(keyFound == true){
                randKey();
                keyFound = false;
            }
        }
    }
       
    window.requestAnimationFrame(gameloop);
}

// Handle Active Browser Tag Animation



window.addEventListener('keydown', input);
// disable the second event listener if you want continuous movement
window.addEventListener('keyup', input);